package main

import (
	"github.com/traefik/paerser/file"
)

type Config struct {
	Addr      string
	ProxyUrls map[string]string
	Metrics   MetricsConfig
}
type MetricsConfig struct {
	Enabled bool
	Addr    string
}

func LoadConfig(path string) (*Config, error) {
	cfg := Config{}
	err := file.Decode(path, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
