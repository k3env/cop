package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

var (
	reqCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "cop_requests_total",
	})
)

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "02.01.06 15:04:05"})

	c := alice.New()
	c = c.Append(hlog.NewHandler(log.Logger))
	c = c.Append(hlog.AccessHandler(func(r *http.Request, status, size int, duration time.Duration) {
		var e *zerolog.Event
		if status > 399 {
			e = hlog.FromRequest(r).Error()
		} else {
			e = hlog.FromRequest(r).Info()
		}
		e.
			Str("method", r.Method).
			Stringer("url", r.URL).
			Int("status", status).
			Int("size", size).
			Dur("duration", duration).
			Msg("")
	}))
	c = c.Append(hlog.RemoteAddrHandler("ip"))
	c = c.Append(hlog.UserAgentHandler("user_agent"))
	c = c.Append(hlog.RefererHandler("referer"))
	c = c.Append(hlog.RequestIDHandler("req_id", "Request-Id"))
	var path string
	flag.StringVar(&path, "path", "", "path to config")
	flag.Parse()
	log.Info().Str("path", path).Msg("Loading config")
	config, err := LoadConfig(path)
	if err != nil {
		log.Fatal().Err(err).Msg("Error loading config")
	}
	r := mux.NewRouter()

	for prefix, u := range config.ProxyUrls {
		counter := promauto.NewCounterVec(prometheus.CounterOpts{
			Name:        "cop_requests",
			ConstLabels: map[string]string{"prefix": prefix},
		},
			[]string{"path", "method", "proto", "status", "response_code"})
		urlPrefix := "/" + prefix + "/"
		h := &ProxyRequest{url: u, counter: counter}
		log.Info().Str("url", u).Str("prefix", prefix).Msg("Registering proxy handler")
		r.PathPrefix(urlPrefix).Handler(http.StripPrefix(urlPrefix, h))
	}

	if config.Metrics.Enabled {
		go func() {
			mr := mux.NewRouter()
			mr.PathPrefix("/metrics").Handler(promhttp.Handler())
			ms := http.Server{Addr: config.Metrics.Addr, Handler: c.Then(mr)}
			log.Info().Str("addr", config.Metrics.Addr).Msg("Starting metrics server")
			log.Fatal().Err(ms.ListenAndServe()).Msg("Error starting metrics server")
		}()
	}
	srv := http.Server{Addr: config.Addr, Handler: c.Then(r)}

	log.Info().Str("addr", config.Addr).Msg("Starting proxy server")
	log.Fatal().Err(srv.ListenAndServe()).Msg("Error starting proxy server")
}

type ProxyRequest struct {
	url     string
	counter *prometheus.CounterVec
}

func (h *ProxyRequest) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := &http.Client{}
	qs := r.URL.RawQuery
	path := r.URL.Path
	apiurl, _ := url.Parse(fmt.Sprintf(h.url, path))
	apiurl.RawQuery = qs
	apiReq := &http.Request{
		Method: r.Method,
		URL:    apiurl,
		Header: r.Header,
		Body:   r.Body,
	}

	res, err := c.Do(apiReq)
	if err != nil {
		log.Print(err)
	}

	for hkey, hvals := range res.Header {
		for _, hval := range hvals {
			w.Header().Set(hkey, hval)
		}
	}
	data, err := io.ReadAll(res.Body)
	if err != nil {
		log.Print(err)
	}
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(res.StatusCode)
	w.Write(data)
	reqCounter.Inc()
	h.counter.With(prometheus.Labels{"path": path, "method": r.Method, "proto": r.Proto, "status": res.Status, "response_code": strconv.Itoa(res.StatusCode)}).Inc()

}
