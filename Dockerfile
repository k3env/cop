FROM golang:alpine AS build
WORKDIR /build
COPY . .
RUN go mod tidy && go build .

FROM ubuntu:latest
WORKDIR /app
COPY --from="build" /build/cop .
ENTRYPOINT "/app/cop"